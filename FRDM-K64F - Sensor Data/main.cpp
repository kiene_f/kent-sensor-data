/*
** My program allows to display on the LCD and on the terminal:
**  - Temperature in Celsius or Fahrenheit
**  - The different degrees of rotation from the accelerometer
**  - The coordinates of the magnetometer and a heading to North
** To do this, it consists of an interface which allows to move in the 3 different menu (Temperature, Accelerometer and Magnetometer).
** The interface also makes it possible to change the speed of refreshment and also makes color changes on the LEDs according to the interactions.
**
** Joystick: push = Change Celsius to Fahrenheit | up = Previous menu | down = Next menu | left = Decrease refresh | right = Increase refresh
*/

#include "mbed.h"
#include "menu.h"

// Menu
Menu menu;

// Start the menu, check if a button is press, display the current level menu and get the refresh rate.
int main ()
{    
    menu.start();
    while (1) {
        menu.checkButton();
        menu.displayLevelMenu();
        wait(menu.getRefreshRate());
    }
}
