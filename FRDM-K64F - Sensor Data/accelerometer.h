#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

struct Angle {
    double x;
    double y;
    double z;
};

double square(double);

#ifndef DEF_ACCELEROMETER
#define DEF_ACCELEROMETER

/*
** The Accelerometer class manages the accelerometer and converts its data into degrees.
*/ 
class Accelerometer
{
    public:
        Accelerometer();
        ~Accelerometer();
        Angle getDegrees();
    
    private:
        Angle convertToDegrees(double, double, double);
};

#endif