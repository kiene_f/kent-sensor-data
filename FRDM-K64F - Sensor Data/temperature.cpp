#include "mbed.h"
#include "LM75B.h"
#include "temperature.h"

// Temperature sensor
LM75B sensor(D14,D15);

Temperature::Temperature() : type(false)
{}

Temperature::~Temperature()
{}

// Get the current temperature in Celsius or Fahrenheit
double Temperature::getTemperature()
{
    this->temp = sensor.temp();
    if (this->type)
        this->temp = this->temp * 1.8 + 32.0;
    return (this->temp);
}

// Defines the type of scale (Celsius or Fahrenheit)
void Temperature::setType(bool type)
{
    this->type = type;
}

// Return the type of scale (Celsius or Fahrenheit)
bool Temperature::getType()
{
    return (this->type);
}