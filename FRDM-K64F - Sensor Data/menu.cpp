#include "mbed.h"
#include "menu.h"
#include "temperature.h"
#include "accelerometer.h"
#include "magnetometer.h"
#include "C12832.h"

// Joystick
DigitalIn up(A2);
DigitalIn down(A3);
DigitalIn left(A4);
AnalogIn right(A5);
DigitalIn push(D4);

// Led
DigitalOut red_led(D5);
DigitalOut blue_led(D8);
DigitalOut green_led(D9);

// Lcd
C12832 display(D11, D13, D12, D7, D10);

// Host pc display
Serial host(USBTX, USBRX);

// Temperature
Temperature temperature;

// Accelerometer
Accelerometer accelerometer;

// Magnetometer
Magnetometer magnetometer;

Menu::Menu() : refresh(1), level(1), maxLevel(3)
{
}

Menu::~Menu()
{}

// Animation when starting the program
void Menu::start()
{
    Menu::led("red");
    display.cls();
    display.locate(50,12);
    display.printf("Hello :)");
    host.printf("Hello :)\n");
    wait(1);
    Menu::led("blue");
    wait(1);
    Menu::led("green");
    wait(1);
    display.cls();
    Menu::led("red");
}

// Function to check if a button is pressed
void Menu::checkButton()
{
    if (right || left || up || down || push)
    {
        Menu::led("blue");
        display.cls();
    }
    if (right && !left && !up && !down && !push)
        Menu::upRefreshRate();
    else if (!right && left && !up && !down && !push)
        Menu::downRefreshRate();
    else if (!right && !left && up && !down && !push)
        Menu::downMenu();
    else if (!right && !left && !up && down && !push)
        Menu::upMenu();
    else if (!right && !left && !up && !down && push && this->level == 1)
        temperature.setType(!temperature.getType());
}

// Return the current refresh rate
unsigned short int Menu::getRefreshRate()
{
    return (this->refresh);
}

// Display the refresh rate
void Menu::displayRefreshRate()
{   
    display.locate(20,12);
    display.printf("Refresh rate = %.d", this->refresh);
    host.printf("Refresh rate = %.d\n", this->refresh);
    wait(1);
    display.cls();
    
    Menu::led("red");
}

// Call the appropriate method according to the menu level of the interface
void Menu::displayLevelMenu()
{   
    if (this->level == 1)
        Menu::displayTemperature();
    else if (this->level == 2)
        Menu::displayAccelerometer();
    else if (this->level == 3)
        Menu::displayMagnetometer();
}

// Displays data related to the magnetometer
void Menu::displayMagnetometer()
{
    Angle angle;
    double north;
    
    angle = magnetometer.getValues();
    north = magnetometer.getNorth(angle.x, angle.y, angle.z);
    
    display.locate(0,0);
    display.printf("-------MAGNETOMETER------");
    host.printf("-------MAGNETOMETER------\n");
    display.locate(0,10);
    display.printf("x=%.0f y=%.0f z=%.0f ", angle.x, angle.y, angle.z);
    host.printf("x=%.0f y=%.0f z=%.0f\n", angle.x, angle.y, angle.z);
    display.locate(0,20);
    display.printf("north=%.2f    ", north);
    host.printf("north=%f\n", angle.x, angle.y, angle.z);
    
    if (north <= 5.0 && north >= -5.0)
        Menu::led("green");
    else if (north <= 35.0 && north >= -35.0)
        Menu::led("blue");
    else
        Menu::led("red");
}

// Displays data related to the accelerometer
void Menu::displayAccelerometer()
{
    Angle angle;
    
    angle = accelerometer.getDegrees();
    display.locate(0,0);
    display.printf("-------ACCELEROMETER------");
    host.printf("-------ACCELEROMETER------\n");
    display.locate(5,14);
    display.printf("x=%.2f y=%.2f z=%.2f ", angle.x, angle.y, angle.z);
    host.printf("x=%.2f y=%.2f z=%.2f\n", angle.x, angle.y, angle.z);
    
    Menu::led("red");
}

// Displays data related to the temperature
void Menu::displayTemperature()
{
    display.locate(0,0);
    display.printf("--------TEMPERATURE--------");
    host.printf("--------TEMPERATURE--------\n");
    if (temperature.getType())
    {
        display.locate(30,14);
        display.printf("%.1f Fahrenheit\n", temperature.getTemperature());
        host.printf("%.1f Fahrenheit\n", temperature.getTemperature());
    }
    else
    {
        display.locate(38,14);
        display.printf("%.1f Celsius\n", temperature.getTemperature());
        host.printf("%.1f Celsius\n", temperature.getTemperature());
    }
    
    Menu::led("red");
}

// Increase refresh rate
void Menu::upRefreshRate()
{
    this->refresh += 1;
    if (this->refresh > 5)
        this->refresh = 5;
    Menu::displayRefreshRate();
}

// Decrease refresh rate
void Menu::downRefreshRate()
{
    if (this->refresh > 1)
        this->refresh -= 1;
    Menu::displayRefreshRate();
}

// Changing the menu level in the interface to the next level
void Menu::upMenu()
{
    if (this->level < this->maxLevel)
        this->level += 1;
    else if (this->level >= this->maxLevel)
        this->level = 1;
    Menu::displayLevelMenu();
}

// Changing the menu level in the interface to the previous level
void Menu::downMenu()
{
    if (this->level > 1)
        this->level -= 1;
    else if (this->level <= 1)
        this->level = this->maxLevel;
    Menu::displayLevelMenu();
}

// Function to simplify the color change of the led
void Menu::led(string color)
{
    red_led = true;
    blue_led = true;
    green_led = true;
    
    if (color == "blue")
        blue_led = false;
    else if (color == "red")
        red_led = false;
    else if (color == "green")
        green_led = false;
}