#ifndef DEF_MENU
#define DEF_MENU

#include <string> 
using namespace std;

/*
** The Menu class represents the program's display interface.
** It also handles the management of buttons and LEDs.
*/
class Menu
{
    public:
        Menu();
        ~Menu();
        void checkButton();
        void start();
        void displayLevelMenu();
        unsigned short int getRefreshRate();
        
    private:
        void displayRefreshRate();
        void displayTemperature();
        void displayAccelerometer();
        void displayMagnetometer();
        void upRefreshRate();
        void downRefreshRate();
        void upMenu();
        void downMenu();
        void led(string);
        
        unsigned short int refresh;
        unsigned short int level;
        unsigned short int maxLevel;
};

#endif