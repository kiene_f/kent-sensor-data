#include "mbed.h"
#include "FXOS8700CQ.h"
#include "accelerometer.h"
#include "magnetometer.h"

// Accelerometer & Magnetometer (6 axis)
FXOS8700CQ fxos(PTE25, PTE24, FXOS8700CQ_SLAVE_ADDR1);
FXOSData_t fx;

Serial test(USBTX, USBRX);

Magnetometer::Magnetometer()
{
}

Magnetometer::~Magnetometer()
{}

// Returns from the 3 coordinates of the magnetometer the orientation in degrees towards the north.
double Magnetometer::getNorth(double x, double y, double z)
{   
    double north;
    
    north = atan2(y, x) * 180.00/M_PI;
    
    return north;
}

// Returns the 3 coordinates of the magnetometer
Angle Magnetometer::getValues()
{
    Angle angle;
    
    fxos.get_data(&fx);
    fxos.enable();
    fxos.get_accel_scale();
    
    angle.x = (double)fx.s.mag_x;
    angle.y = (double)fx.s.mag_y;
    angle.z = (double)fx.s.mag_z;
    
    test.printf("%f\n", Magnetometer::getNorth(angle.x, angle.y, angle.z));
    
    return (angle);
}