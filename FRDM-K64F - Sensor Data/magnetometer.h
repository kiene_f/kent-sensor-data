#ifndef DEF_MAGNETOMETER
#define DEF_MAGNETOMETER

/*
** The Magnetometer class manages the Magnetometer and makes it possible to orient towards the north.
*/
class Magnetometer
{
    public:
        Magnetometer();
        ~Magnetometer();
        Angle getValues();
        double getNorth(double, double, double);
};

#endif