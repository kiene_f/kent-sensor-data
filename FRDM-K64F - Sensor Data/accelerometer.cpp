#include "mbed.h"
#include "MMA7660.h"
#include "accelerometer.h"

// Accelerometer 
MMA7660 MMA(D14,D15);

// Square function
double square(double nb)
{
    return(pow(nb, 2.0));
}

Accelerometer::Accelerometer()
{}

Accelerometer::~Accelerometer()
{}

// The function converts 3 coordinates to degrees
Angle Accelerometer::convertToDegrees(double x, double y, double z)
{   
    Angle angle;
    
    angle.x = atan( x / (sqrt(square(y) + square(z))));
    angle.y = atan( y / (sqrt(square(x) + square(z))));
    angle.z = atan( sqrt(square(x) + square(y)) / z);
    
    angle.x = angle.x * 180.00/M_PI;
    angle.y = angle.y * 180.00/M_PI;
    angle.z = angle.z * 180.00/M_PI;
    
    return (angle);
}

// Return the 3 coordinates of the accelerometer in degrees
Angle Accelerometer::getDegrees()
{
    return Accelerometer::convertToDegrees(MMA.x(), MMA.y(), MMA.z());
}