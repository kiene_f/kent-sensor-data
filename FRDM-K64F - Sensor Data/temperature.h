#ifndef DEF_TEMPERATURE
#define DEF_TEMPERATURE

/*
** The Temperature class manages the temperature in order to return it either in Celsius or in Fahrenheit.
*/
class Temperature
{
    public:
        Temperature();
        ~Temperature();
        double getTemperature();
        void setType(bool);
        bool getType();
        
    private:
        bool type;
        double temp;
};

#endif